#! /bin/bash

echo -n "Quelle largeur voulez-vous? "

read largeur

echo -n "Quelle hauteur voulez-vous? "

read hauteur

echo -n "Quelle type d'image est-ce? "

read type_img

if [ $largeur ] && [ $hauteur ] && [ $type_img ]; then

    sortie=$largeur'x'$hauteur
    suffixe=$sortie

    if [ -d $sortie ]; then

        echo -n "Voulez-vous supprimer le dossier $sortie? "

        read reponse

        case "$reponse" in

            oui ) rm -R $sortie && mkdir $sortie && echo "Dossier $sortie supprimé et recréé.";;

            non ) echo "Dossier $sortie conservé.";;

        esac

    else

        mkdir $sortie
        echo "Dossier $sortie créé"

    fi    

    for src_img in $(find *.$type_img); do
    
        nom_img=${src_img%%.*}
        ext_img=${src_img#*.}
        image=$nom_img'-'$suffixe'.'$ext_img

        cp $src_img $sortie'/'$image

        #mogrify -gravity center -crop $largeur'x'$hauteur'+0+0' $sortie'/'$image -trim

        if [ $largeur -ge $hauteur ]; then
        
            convert $src_img -resize $largeur"x" "$sortie/$image"
        
        elif [ $largeur -le $hauteur ]; then

            convert $src_img -resize "x"$hauteur "$sortie/$image"

        else

            convert $src_img -resize $largeur"x"$hauteur "$sortie/$image" 

        fi

        echo -e "Redimensionnement de" $src_img "en \n" $image "effectué."

    done

    ratio_origine=$(echo "scale=6; $largeur/$hauteur" | bc -l)

    cd "$sortie/"

    for img in $(find *.jpg); do

    largeur_nouvelle=$(identify -format %w $img)
    hauteur_nouvelle=$(identify -format %h $img)

    ratio=$(echo "scale=6; $largeur_nouvelle/$hauteur_nouvelle" | bc -l)
    
        if [ "$ratio" != "$ratio_origine" ]; then

            mogrify -gravity center -crop $largeur'x'$hauteur'+0+0' $image -trim

        
        fi

        if [ "$ratio" != "$ratio_origine" ]; then

            echo "$img sera supprimé."
                rm $img

        fi

    done

else

    echo "Argument non valide, \n
    veuillez entrer redimensionnement-image-lot help dans le terminal."

fi

if [ $# = "1" ] && [ $1 = "help" ]; then
    
    echo -e "Ce script permet de redimensionner un lot d\'images pour cela, entrez dans un terminal :\n
    redimensionnement-image-lot. \n
    Ensuite indiquez la largeur et hauteur voulue, puis le type correspondant aux images.\n
    Les images se trouve dans un dossier \'largeurxhauteur\'."

fi